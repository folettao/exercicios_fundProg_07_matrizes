/*******************************************************************************
 *                     GNU GENERAL PUBLIC LICENSE
 *                        Version 3, 29 June 2007
 *
 *  Copyright (C) 2007 Free Software Foundation, Inc. <https://fsf.org/>
 *  Everyone is permitted to copy and distribute verbatim copies
 *  of this license document, but changing it is not allowed.
 *
 *                             Preamble
 *
 *   The GNU General Public License is a free, copyleft license for
 * software and other kinds of works.
 ******************************************************************************/

package code;

import java.util.Scanner;

/**
 * @author neo
 * @create 21/08/2019
 */
public class Ex_01 {
  public static void main(String[] args) {
    Scanner imput = new Scanner(System.in);

    int[][] matriz;
    int tamanhoMatriz = 0;
    int soma          = 0;
    float media       = 0;
    int maior         = 0;
    int menor         = 0;
    int par           = 0;
    int impar         = 0;
    int diagonalPrinc = 0;
    int diagonalSec   = 0;

    System.out.println("\nInferindo valores sobre Matriz Quadrada.");
    System.out.print("Informe valor entre de 3 a 11: ");
    tamanhoMatriz = (int)imput.nextDouble();
    while (tamanhoMatriz < 3 || tamanhoMatriz > 11)
      tamanhoMatriz = (int)imput.nextDouble();

    matriz = new int[tamanhoMatriz][tamanhoMatriz];

    System.out.printf("Matriz %dx%d formada, complete-a com os elementos\n", tamanhoMatriz, tamanhoMatriz);
    for (int i = 0; i < tamanhoMatriz; i++)
      for (int j = 0; j < tamanhoMatriz; j++)
        matriz[i][j] = (int)imput.nextDouble();

    for (int i = 0; i < tamanhoMatriz; i++) {
      for (int j = 0; j < tamanhoMatriz; j++) {
        // Diagonal Principal
        if (i == j)
          diagonalPrinc += matriz[i][j];

        // Diagona Secundária
        if ((i + j) == (tamanhoMatriz - 1))
          diagonalSec += matriz[i][j];

        // Soma
        soma += matriz[i][j];
        // Maior e Menor
        if (i == 0 && j == 0)
          maior = menor = matriz[i][j];
        else {
          if (maior < matriz[i][j])
            maior = matriz[i][j];
          if (menor > matriz[i][j])
            menor = matriz[i][j];
        }

        // Par e Ímpar
        if (matriz[i][j] % 2 == 0)
          par++;
        else
          impar++;
      }
    }

    media = soma / (tamanhoMatriz * 2);

    /*

    Soma dos elementos;
    Média dos elementos (duas casas decimais);
    Maior valor;
    Menor valor;
    Contador de pares;
    Contador de ímpares;
    Soma da diagonal principal;
    Soma da diagonal secundária;
    Mostrar a Matriz (formatada);

     */
    System.out.println("\nResultado");
    for (int i = 0; i < tamanhoMatriz; i++) {
      for (int j = 0; j < tamanhoMatriz; j++)
        System.out.printf("%d ", matriz[i][j]);
      System.out.println();
    }
    System.out.printf("\nSoma dos elementos: %d", soma);
    System.out.printf("\nMédia dos elementos: %.2f", media);
    System.out.printf("\nMaior valor: %d", maior);
    System.out.printf("\nMenor valor: %d", menor);
    System.out.printf("\nPares: %d", par);
    System.out.printf("\nÍmpares: %d", impar);
    System.out.printf("\nSoma diagonal principal: %d", diagonalPrinc);
    System.out.printf("\nSoma diagonal secundária: %d", diagonalSec);

  }
}
